package server.controller;

import common.Catalog;
import common.ClientRemote;
import common.FileDataTransferObject;
import common.UserDataTransferObject;
import org.mindrot.jbcrypt.BCrypt;
import server.integration.FileDataAccessObject;
import server.integration.UserDataAccessObject;
import server.model.File;
import server.model.FileManager;
import server.model.User;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.List;

public class Controller extends UnicastRemoteObject implements Catalog {
    private final UserDataAccessObject userDAO;
    private final FileDataAccessObject fileDAO;
    private HashMap<Integer, ClientRemote> notifiableUsers = new HashMap<>();
    private HashMap<Integer, ClientRemote> usersOnline = new HashMap<>();

    public Controller() throws RemoteException {
//        super();
        this.userDAO = new UserDataAccessObject();
        this.fileDAO = new FileDataAccessObject();
    }

    @Override
    public void registerUser(String username, String password) throws RemoteException {
        if (userDAO.getUserByName(username) == null) {
            userDAO.saveUser(new User(username, password));
        } else {
            throw new RemoteException("The username '" + username + "' is already taken!");
        }
    }

    @Override
    public void unregisterUser(String username, String password) {
        User user = userDAO.getUserByName(username);
        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            userDAO.removeUser(user);
        }
    }

    @Override
    public UserDataTransferObject loginUser(String username, String password, ClientRemote outputHandler) throws RemoteException {
        User user = userDAO.getUserByName(username);
        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            usersOnline.put(user.getId(), outputHandler);
            return user;
        } else {
            throw new RemoteException("Invalid credentials!");
        }
    }

    public void logoutUser(int id) {
        usersOnline.remove(id);
    }

    @Override
    public List<? extends FileDataTransferObject> findAllFiles() {
        return findAllFiles(null);
    }

    @Override
    public List<? extends FileDataTransferObject> findAllFiles(UserDataTransferObject owner) {
        User user = owner == null ? null : userDAO.getUserByName(owner.getUsername());
        return fileDAO.getAllFiles(user);
    }

    @Override
    public void storeFile(UserDataTransferObject owner, String name, byte[] content, boolean access, boolean write, boolean read) throws IOException {
        if (fileDAO.getFileByName(name) == null) {
            fileDAO.saveFile(new File(userDAO.getUserByName(owner.getUsername()), name, access, write, read, content.length));
            FileManager.persistFile(name, content);
        } else {
            throw new RemoteException("The file '" + name + "' already exists!");
        }

    }

    @Override
    public byte[] getFile(UserDataTransferObject userDTO, String name) throws IOException {
        User user = userDAO.getUserByName(userDTO.getUsername());
        File file = fileDAO.getFileByName(name);

        if (file == null) {
            throw new RemoteException("The file '" + name + "' does not exists!");
        } else if (file.getOwner().getId() == user.getId() || (!file.hasPrivateAccess() && file.hasReadPermission())) {
            if (usersOnline.containsKey(file.getOwner().getId())) notifyClient(file.getOwner().getId(), file.getName(), "downloaded");
            return FileManager.getFile(name);
        } else {
            throw new RemoteException("You don't have the permission to download this file");
        }
    }

    @Override
    public void updateFile(UserDataTransferObject owner, String name, byte[] content, boolean access, boolean write, boolean read) throws IOException {
        User user = userDAO.getUserByName(owner.getUsername());
        File file = fileDAO.getFileByName(name);

        if (file == null) {
            throw new RemoteException("The file '" + name + "' does not exists!");
        } else if (file.getOwner().getId() == user.getId() || (!file.hasPrivateAccess() && file.hasWritePermission())) {
            fileDAO.updateFile(new File(userDAO.getUserByName(owner.getUsername()), name, access, write, read, content.length));
            if (usersOnline.containsKey(file.getOwner().getId())) notifyClient(file.getOwner().getId(), file.getName(), "updated");
            FileManager.persistFile(name, content);
        } else {
            throw new RemoteException("You don't have the permission to update this file");
        }
    }

    @Override
    public void deleteFile(UserDataTransferObject userDTO, String name) throws IOException {
        User user = userDAO.getUserByName(userDTO.getUsername());
        File file = fileDAO.getFileByName(name);

        if (file == null) {
            throw new RemoteException("The file '" + name + "' does not exists!");
        } else if (file.getOwner().getId() == user.getId() || (!file.hasPrivateAccess() && file.hasWritePermission())) {
            fileDAO.removeFile(file);
            if (usersOnline.containsKey(file.getOwner().getId())) notifyClient(file.getOwner().getId(), file.getName(), "deleted");
            FileManager.deleteFile(name);
        } else {
            throw new RemoteException("You don't have the permission to delete this file");
        }
    }

    @Override
    public void notify(UserDataTransferObject userDTO, String name, ClientRemote outputHandler) throws RemoteException {
        System.out.println("Works?");
        User user = userDAO.getUserByName(userDTO.getUsername());
        File file = fileDAO.getFileByName(name);

        if (file == null) {
            throw new RemoteException("The file '" + name + "' does not exists!");
        } else if (file.getOwner().getId() == user.getId()) {
            notifiableUsers.put(file.getId(), outputHandler);
        } else {
            throw new RemoteException("You don't have the permission be notified!");
        }
    }

    private void notifyClient(int id, String fileName, String action) throws RemoteException {
        ClientRemote outputHandler = usersOnline.get(id);
        outputHandler.outputMessage("A user has " + action + " the file " + fileName);
    }
}
