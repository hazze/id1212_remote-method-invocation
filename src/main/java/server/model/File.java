package server.model;

import common.FileDataTransferObject;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Date;

@Entity
public class File implements FileDataTransferObject {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private User owner;

    @NaturalId
    private String name;

    private boolean privateAccess;

    private boolean readPermission;

    private boolean writePermission;

    private long dimension;

    private Date createdAt;

    private Date updatedAt;

    public File(User owner, String name, boolean privateAccess, boolean writePermission, boolean readPermission, long dimension) {
        this.owner = owner;
        this.name = name;
        this.privateAccess = privateAccess;
        this.readPermission = readPermission;
        this.writePermission = writePermission;
        this.dimension = dimension;
    }

    public File() {
    }

    @PrePersist
    private void onCreate() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @PreUpdate
    private void onUpdate() {
        this.updatedAt = new Date();
    }

    public int getId() {
        return id;
    }

    public User getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public boolean hasPrivateAccess() {
        return privateAccess;
    }

    public boolean hasReadPermission() {
        return readPermission;
    }

    public boolean hasWritePermission() {
        return writePermission;
    }

    public long getDimension() {
        return dimension;
    }
}
