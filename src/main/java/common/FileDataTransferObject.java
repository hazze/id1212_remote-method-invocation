package common;

import server.model.User;

import java.io.Serializable;
import java.util.Date;

public interface FileDataTransferObject extends Serializable {
    User getOwner();

    String getName();

    boolean hasPrivateAccess();

    boolean hasReadPermission();

    boolean hasWritePermission();

    long getDimension();
}
