package common;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Catalog extends Remote {

    void registerUser(String username, String password) throws RemoteException;

    void unregisterUser(String username, String password) throws RemoteException;

    UserDataTransferObject loginUser(String username, String password, ClientRemote outputHandler) throws RemoteException;

    void logoutUser(int id) throws RemoteException;

    List<? extends FileDataTransferObject> findAllFiles(UserDataTransferObject owner) throws RemoteException;

    List<? extends FileDataTransferObject> findAllFiles() throws RemoteException;

    void storeFile(UserDataTransferObject owner, String name, byte[] content, boolean access, boolean write, boolean read) throws IOException;

    byte[] getFile(UserDataTransferObject user, String name) throws IOException;

    void updateFile(UserDataTransferObject owner, String name, byte[] content, boolean access, boolean write, boolean read) throws IOException;

    void deleteFile(UserDataTransferObject user, String name) throws IOException;

    void notify(UserDataTransferObject user, String name, ClientRemote outputHandler) throws RemoteException;
}
