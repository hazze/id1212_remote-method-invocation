package common;

import server.model.File;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface UserDataTransferObject extends Serializable {

    int getId();

    String getUsername();

    String getPassword();
//
//    List<File> getFiles();
//
//    Date getCreatedAt();
//
//    Date getUpdatedAt();

}
