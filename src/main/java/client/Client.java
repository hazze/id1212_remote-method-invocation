package client;

import client.view.Shell;
import common.Catalog;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Client {

    public static void main(String[] args) {
        try {
            Catalog catalog = (Catalog) Naming.lookup("catalog");
            new Shell().start(catalog);
        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            System.out.println("Could not start catalog shell!");
        }
    }
}