package client.view;

import java.util.ArrayList;
import java.util.StringTokenizer;

class LineParser {
    private ArrayList<String> args = new ArrayList<>();
    private Command command;

    LineParser(String input) {
        parseCommand(input);
    }

    private void parseCommand(String input) {
        StringTokenizer stringTokenizer = new StringTokenizer(input);

        if (stringTokenizer.countTokens() == 0) {
            this.command = Command.NO_COMMAND;
            return;
        }

        String cmd = stringTokenizer.nextToken().toUpperCase();
        switch (cmd) {
            case "REGISTER":
                this.command = Command.REGISTER;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing username");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing password");
                args.add(stringTokenizer.nextToken());
                break;
            case "UNREGISTER":
                this.command = Command.UNREGISTER;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing username");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing password");
                args.add(stringTokenizer.nextToken());
                break;
            case "LOGIN":
                this.command = Command.LOGIN;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing username");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing password");
                args.add(stringTokenizer.nextToken());
                break;
            case "LOGOUT":
                this.command = Command.LOGOUT;
                break;
            case "UPLOAD":
                this.command = Command.STORE_FILE;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing name");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing permission");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing public write");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing public read");
                args.add(stringTokenizer.nextToken());
                break;
            case "DOWNLOAD":
                this.command = Command.GET_FILE;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing file name");
                args.add(stringTokenizer.nextToken());
                break;
            case "UPDATE":
                this.command = Command.UPDATE_FILE;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing name");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing permission");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing public write");
                args.add(stringTokenizer.nextToken());
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing public read");
                args.add(stringTokenizer.nextToken());
                break;
            case "REMOVE":
                this.command = Command.DELETE_FILE;
                if (!stringTokenizer.hasMoreTokens()) throw new IllegalArgumentException("Missing file name");
                args.add(stringTokenizer.nextToken());
                break;
            case "LIST":
                this.command = Command.LIST_FILES;
                break;
            case "QUIT":
                this.command = Command.QUIT;
                break;
            default:
                this.command = Command.HELP;
        }
    }

    String getArgument(int index) {
        return args.get(index);
    }

    Command getCommand() {
        return command;
    }
}