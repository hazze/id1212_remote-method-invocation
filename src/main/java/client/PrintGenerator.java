package client;

public class PrintGenerator {
    public static String initMsg() {
        return "## File Catalog started ##\n";
    }

    public static String errorMsg(String reason) {
        return "Command error: " + reason + "\n";
    }

    public static String simpleMsg(String message) {
        return message + "\n";
    }

    public static String infoMsg() {
        return "Type 'help' to display available commands!\n" +
                "Cancel the session with 'quit'" + "\n";
    }

    public static String helpMsg() {
        return  "Available commands: \n" +
                "   register <username> <password>\n" +
                "   unregister <username> <password>\n" +
                "   login <username> <password>\n" +
                "   logout\n" +
                "   upload <name> <private(t/f)> <write(t/f)> <read(t/f)>\n" +
                "   download <name>\n" +
                "   remove <name>\n" +
                "   update <name> <new permissions <private(t/f)> <write(t/f)> <read(t/f)>>\n" +
                "   list\n";
    }
}